package reshef.mlflame.ml

import org.apache.spark.sql.types.StructType

/**
 * Created by roir on 03/08/2016.
 */
trait UseParamValidators {
  protected val validators: scala.collection.mutable.Map[String, StructType => Unit]
}

trait HasParamValidation extends UseParamValidators{
  override protected val validators = scala.collection.mutable.Map[String, StructType => Unit]()
  def validateSchema(schema: StructType) = validators.keys.foreach(validateField(schema,_))
  def validateField(schema: StructType, field: String) = validators(field).apply(schema)
}