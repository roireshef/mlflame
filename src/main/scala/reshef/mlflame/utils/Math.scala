package reshef.mlflame.utils

object Math {
  /** power operation for Ints **/
  def pow(base: Int, power: Int): Int = if (power==0) 1 else base * pow(base, power-1)

  /** log (base b) of x **/
  def log(x: Double, b: Double) = math.log(x)/math.log(b)
}
