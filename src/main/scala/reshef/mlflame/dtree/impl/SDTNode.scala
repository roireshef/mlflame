package reshef.mlflame.dtree.impl

import reshef.mlflame.utils.Reflection
import reshef.mlflame.utils.String.indentOnce
import reshef.mlflame.dtree.base.SDTNode
import reshef.mlflame.dtree.dto.{SlimLabeledPoint, SlimSparseVector}
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD


/**
  * Splitting node implementation for SparseDecisionTree
  */
class SDTSplitNode[FV,CV](
                           override val stats: SplitStats[FV,CV],
                           lChild: Some[SDTNode[FV]],
                           rChild: Some[SDTNode[FV]])(implicit fNum:Numeric[FV])
  extends SDTNode(stats, lChild, rChild) with Serializable{
  override def toString() = s"SCDTreeSplitNode($stats)[\n" +
    indentOnce(lChild.toString) + "\n" +
    indentOnce(rChild.toString) + "\n]"

  protected[dtree]
  override def predict(fVector: SlimSparseVector[FV]): (Int, Double) = {
    val featureValue = fVector.apply(stats.featureIndex).getOrElse(fNum.zero)
    if (stats.leftSubsetValues.contains(featureValue))
      lChild.get.predict(fVector)
    else if (stats.rightSubsetValues.contains(featureValue))
      rChild.get.predict(fVector)
    else
      throw new ArrayIndexOutOfBoundsException(s"Feature value $featureValue can not be found in neither children of $this")
  }
}

/**
  * Leaf node implementation class for SparseDecisionTree
  */
class SDTLeafNode[FV, CV: Ordering](
                                        override val stats: LeafStats[CV]
                                      ) extends SDTNode[FV](stats, None, None) with Serializable{
  override def toString() = s"SCDTreeLeafNode($stats)"

  protected[dtree]
  override def predict(fVector: SlimSparseVector[FV]): (Int, Double) =
    stats.probability.zipWithIndex.map(_.swap).maxBy(_._2)
}

/**
  *
  * @param underlying the underlying node (can be either a splitting-node or a leaf)
  * @tparam FV        type of features
  */
class SDTRoot[FV: Numeric](underlying: SDTNode[FV])
  extends SDTNode[FV](underlying.stats, underlying.lChild, underlying.rChild) with Serializable{
  /** predicts from feature vectors **/
  def predict(data: RDD[SlimSparseVector[FV]]) = SDTRoot.predict(data,this)

  /** predicts from a feature vector **/
  def predict(point: SlimSparseVector[FV]) = underlying.predict(point)

  /** predicts with the actual supervision class feedback attached **/
  def predictWithFeedback(data: RDD[SlimLabeledPoint[FV]]) = SDTRoot.predictWithFeedback(data,this)

  /** predicts with the observation id attached **/
  def predictWithID[ID](data: RDD[(ID,SlimSparseVector[FV])]) = SDTRoot.predictWithID(data,this)

  override def map[U](f:SDTNode[FV]=>Option[U]): Set[Option[U]] = underlying.map(f)
  def flatMap[U](f:SDTNode[FV]=>Option[U]) = map(f).flatten

  override def toString() =
    s"SCDTreeRootNode-${underlying.toString}"
}

private[impl] object SDTRoot{
  def predict[FV, CV](data: RDD[SlimSparseVector[FV]], rootNode: SDTRoot[FV]): RDD[(Int, Double)] = {
    val bcRootNode = data.sparkContext.broadcast(rootNode)

    val result = data.mapPartitions(_.map(bcRootNode.value.predict),true).cache()
    result.count()

    bcRootNode.unpersist(false)
    result
  }
  def predictWithFeedback[FV, CV](data: RDD[SlimLabeledPoint[FV]], rootNode: SDTRoot[FV]) = {
    val bcRootNode = data.sparkContext.broadcast(rootNode)

    val result = data.map(point => (bcRootNode.value.predict(point.features),point.label)).cache()
    result.count()

    bcRootNode.unpersist(false)
    result
  }
  def predictWithID[FV, ID](data: RDD[(ID, SlimSparseVector[FV])], rootNode: SDTRoot[FV]) = {
    val bcRootNode = data.sparkContext.broadcast(rootNode)

    val result = data.map(point => (point._1, bcRootNode.value.predict(point._2))).cache()
    result.count()

    bcRootNode.unpersist(false)
    result
  }
}