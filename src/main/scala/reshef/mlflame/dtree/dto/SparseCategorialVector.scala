package reshef.mlflame.dtree.dto

import org.apache.spark.mllib.linalg.{DenseVector, SparseVector, Vector, Vectors}
import reshef.mlflame.utils.Reflection

import scala.collection.mutable
import scala.reflect.ClassTag

/**
  * Feature vector of categorial values. values are either of type Int or type Long.
  */
case class SparseCategorialVector[@specialized(Long,Int) V:ClassTag](
                                          size: Int,
                                          indices: Array[Int],
                                          values: Array[V]
                                          )(implicit num:Numeric[V])
  extends SlimSparseVector[V] with Serializable with SortedByIndex{

  /** copied from {@link org.apache.spark.mllib.linalg.SparseVector#hashCode} **/
  override def hashCode: Int = {
    var result: Int = 31 + size
    val end = values.length
    var k = 0
    var nnz = 0
    while (k < end && nnz < 128) {
      val v = values(k)
      result = 31 * result + indices(k)
      val bits = java.lang.Double.doubleToLongBits(num.toDouble(v))
      result = 31 * result + (bits ^ (bits >>> 32)).toInt
      nnz += 1
      k += 1
    }
    result
  }

  override def +[V2](that: SlimSparseVector[V2]): SlimSparseVector[V] = {
    require(that.isInstanceOf[SparseCategorialVector[V]],
      s"${that.getClass.getSimpleName} is not a ${getClass.getSimpleName}")

    this.+(that.asInstanceOf[SparseCategorialVector[V]])
  }

  /** sum with another sparse vector efficiently **/
  def +(that: SparseCategorialVector[V]) = {
    require(size == that.size)

    var iThis = 0
    var iThat = 0

    // placeholders for the new vector indices and values
    val newIndices = new mutable.MutableList[Int]()
    val newValues = new mutable.MutableList[V]()

    while (iThis < this.indices.length && iThat < that.indices.length){
      this.indices(iThis) - that.indices(iThat) match{
        case x if x > 0 =>
          newIndices += that.indices(iThat)
          newValues += that.values(iThat)
          iThat += 1
        case x if x < 0 =>
          newIndices += this.indices(iThis)
          newValues += this.values(iThis)
          iThis += 1
        case _ => //x == 0
          newIndices += this.indices(iThis)
          newValues += num.plus(this.values(iThis),that.values(iThat))
          iThat+=1
          iThis+=1
      }
    }

    // merge leftovers from "this" vector
    while (iThis < this.indices.length){
      newIndices += this.indices(iThis)
      newValues += this.values(iThis)
      iThis += 1
    }

    // merge leftovers from "that" vector
    while (iThat < that.indices.length){
      newIndices += that.indices(iThat)
      newValues += that.values(iThat)
      iThat += 1
    }

    SparseCategorialVector(size,newIndices.toArray,newValues.toArray)
  }

  /** get value by index. utilizes <a href="https://en.wikipedia.org/wiki/Binary_search_algorithm">binary search</a>
    * for better performance */
  def apply(index: Int): Option[V] = java.util.Arrays.binarySearch(indices,index) match{
    case i if i>=0 => Some(values(i))
    case _ => None
  }

  /** normalize by total sum to get probabilities **/
  def toProb = {
    val doubles = values.map(num.toDouble)
    val sum = doubles.sum
    Vectors.sparse(size,indices,doubles.map(_ / sum))
  }

  override def toString: String =
    s"SlimSparseVector($size,${indices.mkString("[", ",", "]")},${values.mkString("[", ",", "]")})"

  override def zipped: Stream[(Int, V)] = indices.toStream zip values
}

object SparseCategorialVector{
  /** Additional constructor for SparseCategorialVector that takes a zipped collection as an input**/
  def apply[@specialized(Long,Int)V:ClassTag](size:Int, iter: Iterable[(Int,V)])(implicit num:Numeric[V]) = {
    val (indices, values) = iter.toArray.sortBy(_._1).unzip
    new SparseCategorialVector(size,indices.toArray,values.toArray)
  }

  /** Constructor that takes mllib.linalg.Vector as an input **/
  def fromVector[@specialized(Long,Int) V:ClassTag:Numeric](vec: Vector) = {
    vec match {
      case v: DenseVector => new SparseCategorialVector(v.size, 0 to (v.size - 1) toArray, v.values.map(Reflection.fromDouble[V]))
      case v: SparseVector => {
        val (srtIndices, srtValues) = (v.indices zip v.values sortBy (_._1)).unzip
        new SparseCategorialVector(v.size, srtIndices.toArray, srtValues.toArray.map(Reflection.fromDouble[V]))
      }
    }
  }
}