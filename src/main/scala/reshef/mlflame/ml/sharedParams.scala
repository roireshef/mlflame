package reshef.mlflame.ml

import org.apache.spark.ml.param.{Param, Params}
import org.apache.spark.mllib.linalg.VectorUDT

trait OptPredictionCol extends Params {
  final val optPredictionCol: Param[Option[String]] = new Param[Option[String]](
    this, "optPredictionCol", "prediction column name (do not set if prediction is not required)")

  setDefault(optPredictionCol, None)

  /** @group getParam */
  final def getPredictionCol: Option[String] = $(optPredictionCol)
}

trait OptConfidenceCol extends Params {
  final val optConfidenceCol: Param[Option[String]] = new Param[Option[String]](
    this, "optConfidenceCol", "prediction confidence column name (do not set if confidence is not required)")

  setDefault(optConfidenceCol, None)

  /** @group getParam */
  final def getConfidenceCol: Option[String] = $(optConfidenceCol)
}

trait OptExtractionCol extends Params {
  final val optExtractionCol: Param[Option[String]] = new Param[Option[String]](
    this, "optExtractionCol", "feature extraction column name (do not set if extraction is not required)")

  setDefault(optExtractionCol, None)

  /** @group getParam */
  final def getExtractionCol: Option[String] = $(optExtractionCol)
}

trait HasFeaturesCol extends Params with UseParamValidators{
  /**
   * Param for input column name.
   * @group param
   */
  final val featuresCol: Param[String] = new Param[String](
    this, "featuresCol", "features column name (must hold linalg vector)")

  /** @group getParam */
  final def getFeaturesCol: String = $(featuresCol)

  validators += ("featuresCol", SchemaUtils.checkColumnType(_,$(featuresCol), new VectorUDT ,this.getClass.getSimpleName))
}

trait HasClassCol extends Params with UseParamValidators{
  /**
   * Param for input column name.
   * @group param
   */
  final val classCol: Param[String] = new Param[String](
    this, "classCol", "features column name (must hold numbers)")

  /** @group getParam */
  final def getClassCol: String = $(classCol)

  validators += ("classCol", SchemaUtils.checkNumericType(_,$(classCol), this.getClass.getSimpleName))
}
