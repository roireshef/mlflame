package reshef.mlflame.dtree.base

/**
 * Base object for Impurity function implementations
 */
private[mlflame] object Impurity {
  type ImpurityFunction[CV] = Array[CV] => Double
}
