package reshef.mlflame.dtree.dto

/**
  * Base trait for Tuple2 extensions (different hashing methods).
  */
//making this an abstract class raises a warning
sealed trait Tuple2[
    @specialized(Int, Long, Double, Char, Boolean) +T1,
    @specialized(Int, Long, Double, Char, Boolean) +T2]
    extends Product2[T1, T2] {

  override def hashCode: Int
  def swap: Tuple2[T2, T1]
  override def toString: String = s"${this.getClass.getSimpleName}(${_1}, ${_2})"


}

/**
  * Fully Hashed Tuple2 (two elements). Whereas Tuple2 uses only the first element for self-hashing (and distribution),
  * FHTuple2 uses both elements' hash-codes for self hashing.
  */
case class FHTuple2[
    @specialized(Int, Long, Double, Char, Boolean) +T1,
    @specialized(Int, Long, Double, Char, Boolean) +T2](_1: T1, _2: T2)
  extends Tuple2[T1, T2] with Serializable{
  override def hashCode: Int = _1.hashCode * 31 + _2.hashCode

  /** swaps the elements of this tuple
    * use with caution as this method changes hash code and thus will redistribute tuple across machines */
  def swap: FHTuple2[T2, T1] = FHTuple2(_2, _1)

}

/**
  * Randomly Hashed Tuple2 (two elements). Whereas Tuple2 uses its first element for self-hashing (and distribution),
  * RHTuple2 uses a random number defined at the time of instantiation
  */
case class RHTuple2 [
    @specialized(Int, Long, Double, Char, Boolean) +T1,
    @specialized(Int, Long, Double, Char, Boolean) +T2](_1: T1, _2: T2)
  extends Tuple2[T1, T2] with RandomlyShuffled with Serializable{

  /** swaps the elements of this tuple
    * use with caution as this method changes hash code and thus will redistribute tuple across machines */
  def swap: RHTuple2[T2, T1] = RHTuple2(_2, _1)
}
