package reshef.mlflame.utils

/**
 * Created by roir on 03/08/2016.
 */
object String {
  /** indent each line of the input string by '\t' **/
  def indentOnce(str: String) = str.split('\n').map(x=>s"\t$x").reduceLeft(_ + "\n" + _)
}
