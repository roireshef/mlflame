package reshef.mlflame.dtree.dto

import scala.collection.mutable
import org.apache.spark.mllib.linalg._

/**
  * Feature vector of binary values. It holds only "hot" indices.
  */
case class SparseBinaryVector(size: Int, indices: Array[Int])
  extends SlimSparseVector[Int] with Serializable  with SortedByIndex{
  @transient override lazy val values: Array[Int] = Array.fill(indices.length)(1)

  override def hashCode(): Int = {
    var result: Int = 31 + size
    val end = values.length
    var k = 0
    var nnz = 0
    while (k < end && nnz < 32) {
      result = 31 * result + indices(k)
      nnz += 1
      k += 1
    }
    result
  }

  override def +[V2](that: SlimSparseVector[V2]): SlimSparseVector[Int] = {
    require(that.isInstanceOf[SparseBinaryVector],
      s"${that.getClass.getSimpleName} is not a ${getClass.getSimpleName}")

    this.+(that.asInstanceOf[SparseBinaryVector])
  }

  /** sum this sparse vector with another one efficiently **/
  def +(that: SparseBinaryVector): SparseBinaryVector = {
    require(size == that.size)

    var iThis = 0
    var iThat = 0

    val newIndices = new mutable.MutableList[Int]()

    while (iThis < this.indices.size && iThat < that.indices.size){
      this.indices(iThis) - that.indices(iThat) match{
        case x if x > 0 =>
          newIndices += that.indices(iThat)
          iThat += 1
        case x if x < 0 =>
          newIndices += this.indices(iThis)
          iThis += 1
        case _ =>
          newIndices += this.indices(iThis)
          iThat+=1
          iThis+=1
      }
    }

    while (iThis < this.indices.size){
      newIndices += this.indices(iThis)
      iThis += 1
    }

    while (iThat < that.indices.size){
      newIndices += that.indices(iThat)
      iThat += 1
    }

    SparseBinaryVector(size,newIndices.toArray)
  }

  /** get value by index **/
  override def apply(index: Int): Option[Int] = java.util.Arrays.binarySearch(indices,index) match{
    case i if i>=0 => Some(1)
    case _ => None
  }

  override def toString: String =
    s"BinaryVector($size, ${indices.mkString("[",",","]")}"

  override def zipped: Stream[(Int,Int)] = indices.toStream zip Stream.continually(1)
}

object SparseBinaryVector{
  /** Constructor that takes mllib.linalg.Vector as an input **/
  def fromVector(vec: Vector) = {
    vec match {
      case v: DenseVector => new SparseBinaryVector(v.size, v.values.zipWithIndex.filter(_._1>0).map(_._2))
      case v: SparseVector => new SparseBinaryVector(v.size, v.indices.sorted)
    }
  }
}