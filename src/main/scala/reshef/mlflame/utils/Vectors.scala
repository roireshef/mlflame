package reshef.mlflame.utils

import org.apache.spark.mllib.linalg.{DenseVector, SparseVector, Vector}

/**
 * Created by roir on 03/08/2016.
 */
object Vectors {
  //todo write details
  def extract(v: Vector, indices: Seq[Int]): Vector = v match{
      case sv: SparseVector =>
        val (newIndices, newValues) = (sv.indices zip sv.values).
            map(x=>(indices.indexOf(x._1), x._2)).
            filter(_._1>=0).
            unzip
        new SparseVector(indices.length, newIndices.toArray, newValues.toArray)
      case dv: DenseVector => new DenseVector(indices.map(dv.values(_)).toArray)
    }
}
