import sbt.{Build, File, Project}

object ProjectDependencies {
}

object ProjectBuild extends Build{
  lazy val root = Project(
    id = "mlflame",
    base = new File(".")
  ).dependsOn()
}