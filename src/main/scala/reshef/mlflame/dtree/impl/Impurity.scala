package reshef.mlflame.dtree.impl

import reshef.mlflame.dtree.base.Impurity.ImpurityFunction
import reshef.mlflame.utils.Math
/**
  * Implementations of Impurity functions
  */
object Impurity extends Serializable{
  /**
    * Implementation of <a href="https://en.wikipedia.org/wiki/Entropy_(information_theory)#Definition"> Entropy impurity</a>
    * @param b    the base of the log
    * @tparam CV  type of supervision class
    * @return     an impurity function that operates an a histogram (array of frequencies)
    */
  def Entropy[@specialized(Int, Long) CV](b:Int)(implicit num:Numeric[CV]): ImpurityFunction[CV] =
  (arr:Array[CV]) => {
    val sum = num.toDouble(arr.sum)
    arr.map(x => num.toDouble(x) / sum).map{
      case 0 => 0d
      case p => -p * Math.log(p,b)
    }.sum
  }

  /**
    * Implementation of <a href="https://en.wikipedia.org/wiki/Decision_tree_learning#Gini_impurity">Gini impurity</a>
    * @tparam CV  type of supervision class
    * @return     an impurity function that operates an a histogram (array of frequencies)
    */
  def Gini[@specialized(Int, Long) CV](implicit num:Numeric[CV]): ImpurityFunction[CV] =
    (arr:Array[CV]) => {
      val sum = num.toDouble(arr.sum)
      arr.map(x => num.toDouble(x) / sum).map(p => p * (1-p)).sum
    }
}
