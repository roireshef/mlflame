package reshef.mlflame.dtree

import reshef.mlflame.utils.Math.pow
import reshef.mlflame.dtree.base.Impurity.ImpurityFunction
import reshef.mlflame.dtree.base.SDTNode
import reshef.mlflame.dtree.config.StoppingCriterion
import reshef.mlflame.dtree.dto._
import reshef.mlflame.dtree.impl._
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel
import org.apache.spark.{HashPartitioner, SparkContext}
import reshef.mlflame.utils.Reflection

import scala.math.Ordering
import scala.reflect.ClassTag
import scala.reflect.runtime.universe.TypeTag

/**
  * Sparse Decsion Tree model class for training. This model is implemented as a stage-per-split
  * so that each split is computed in a distributed fashion but splits are computed sequently.
  * The high performance gain is due to the underlying assumption of sparseness in the feature space,
  * and due to efficient distribution of the dataset across the cluster's nodes
  * @see dto.FHTuple2, dto.RHTuple2
  * @param numLabels  number of classes
  * @param stop       stopping criterion for training
  * @param impurity   impurity function
  * @tparam FV        Type of features
  * @tparam CV        Type of predicted (supervision) class
  */
class SparseDecisionTree[@specialized(Int, Long) FV, @specialized(Int, Long) CV: TypeTag: ClassTag](
    numLabels: Int,
    stop: StoppingCriterion,
    impurity: ImpurityFunction[CV])(implicit fNum: Numeric[FV], cNum: Numeric[CV]) extends Serializable{

  // ((label, featInd), (featVal, freq))
  type InterLabelFeatureStats = RHTuple2[(Int, Int), (FV, CV)]
  // ((featInd, Iterable[((subset1Values, subset2Values), (subset1LabelStats, subset2LabelStats))])
  type FeatureSplits = (Int, Iterable[((Set[FV], Set[FV]), (SparseCategorialVector[CV], SparseCategorialVector[CV]))])

  private val defaultPartitioner = new HashPartitioner(SparkContext.getOrCreate.defaultParallelism)

  private val rddToUnpersist = scala.collection.mutable.Queue[RDD[_]]()

  /** METHODS **/

  /**
    * train the decision tree model on a training set
    * @param data the training set - a distributed collection of observations in SlimLabeledPoint format
    * @return a SparseDecisionTree root-node instance
    */
  def run(data: RDD[SlimLabeledPoint[FV]]): SDTRoot[FV] = {
    if (data.getStorageLevel==StorageLevel.NONE) data.setName("SDT_Dataset").cache()
    val labelHist = SparseCategorialVector.apply(numLabels, computeLabelHistogram(data))

    new SDTRoot(splitRecursive(data,labelHist,0))
  }

  /** TRAINING UTILITY FUNCTIONS **/

  /** counts label frequncies in the dataset **/
  private def computeLabelHistogram(dataset: RDD[SlimLabeledPoint[FV]]) =
    dataset.map(_.label).countByValue().mapValues(Reflection.castLongTo[CV]).toArray.sortBy(_._1)

  /** split the training dataset recursively until hitting the stopping criterion **/
  private def splitRecursive(subset: RDD[SlimLabeledPoint[FV]], labelHist: SparseCategorialVector[CV], depth: Int): SDTNode[FV] = {
    // first enumerate all valid splits of the dataset
    val validSplits =
      findSplits(subset, labelHist, depth).
        mapPartitions(_.map(_.filter(stop.isSplitValid[FV,CV])).filter(_.nonEmpty), true).
        cache()

    val numValidSplits = validSplits.count()

    //unpersist all RDDs cached in computeLabelStats
    rddToUnpersist.foreach(_.unpersist(false))
    rddToUnpersist.dequeueAll(_=>true)

    if (numValidSplits>0){      //if number of VALID splits is positive
      // for each feature leave only the best split, then leave the best split across all features
      val bestSplit = validSplits.map(_.maxBy(_.gain)).max()(Ordering.by(_.gain))

      validSplits.unpersist(false)

      val leftSubset =
        subset.filter(_.features(bestSplit.featureIndex) match{
          case Some(v) => bestSplit.leftSubsetValues.contains(v)
          case None => bestSplit.leftSubsetValues.contains(fNum.zero)
        })

      val rightSubset =
        subset.filter(_.features(bestSplit.featureIndex) match{
          case Some(v) => bestSplit.rightSubsetValues.contains(v)
          case None => bestSplit.rightSubsetValues.contains(fNum.zero)
        })

      leftSubset.cache()
      rightSubset.cache()
      subset.unpersist(false)

      new SDTSplitNode(
        bestSplit,
        Some(splitRecursive(leftSubset,bestSplit.leftSubsetHistogram,depth+1)),
        Some(splitRecursive(rightSubset,bestSplit.rightSubsetHistogram,depth+1))
      )
    } else { //no valid splits found
      validSplits.unpersist(false)
      subset.unpersist(false)
      new SDTLeafNode(LeafStats(depth,labelHist,impurity(labelHist.values)))
    }
  }

  /** find all possible and valid splits of the datasets and compute statistics for each **/
  private def findSplits(dataset: RDD[SlimLabeledPoint[FV]], labelHist: SparseCategorialVector[CV], currentDepth: Int) = {
    //compute the impurity of the dataset before the split
    val currentImpurity = impurity.apply(labelHist.values)

    //compute inter-label feature-statistics, i.e.
    //within any class, for each feature - compute a histogram of the feature values
    val labelStats = computeLabelStats(dataset, labelHist.indices zip labelHist.values toMap)

    //within any feature, for each value - compute a histogram of the supervision classes
    val featureValuesStats = labelStats.
      mapPartitions(_.toIterable.map{ case RHTuple2((label,featInd),(featVal,freq)) =>
        (FHTuple2(featInd,featVal),(label,freq))
      }.groupBy(_._1).mapValues(iter => SparseCategorialVector(numLabels, iter.map(_._2))).toIterator
        ,true).reduceByKey(_ + _)

    //enumerate all possible splits and compute their statistics
    val potentialSplits = featureValuesStats.
      map{case (FHTuple2(featInd,featVal),labelStats) => (featInd,(featVal,labelStats))}.
      groupByKey().mapValues(x=>permute(x.toMap)).  //enumerate all possible splits for each feature across all of its existing values
      map{ case (featInd, splits) =>
        splits.map{ case ((subset1FeatVals,subset2FeatVals),(subset1Hist,subset2Hist)) =>
          SplitStats(
            featInd,
            subset1FeatVals,
            subset2FeatVals,
            currentDepth,
            impurity(labelHist.values),
            currentImpurity - InformationGain.computeDelta(impurity)(subset1Hist.values,subset2Hist.values),
            subset1Hist,
            subset2Hist
          )
        }
      }.filter(_.nonEmpty)

    potentialSplits
  }

  /**
    * computes inter-label feature-statistics, i.e. for each pair of (class, feature)
    * it computes a histogram of the actual values of that feature in the specific class context
    * @param dataset              dataset of observations
    * @param totalLabelHistogram  a histogram (map) of the supervision class in the dataset
    * @return                     a randomly hashed RDD of the format [((Label, Feature_Index), (Feature_Value, Frequency))]
    */
  private def computeLabelStats(dataset: RDD[SlimLabeledPoint[FV]], totalLabelHistogram: Map[Int, CV]): RDD[InterLabelFeatureStats] = {
    val bcTotalLabelCounts = dataset.sparkContext.broadcast(totalLabelHistogram)

    //Compute frequencies for each tuple (label, featInd, featVal) where featVal is positive (>0)
    //Note: this doesn't include zero element counts!
    val featWithinLabelDistribution = dataset.
      mapPartitions(_.flatMap{
          case point: SlimLabeledPoint[_] => point.features.zipped.map{case (fInd, fVal) => (point.label, fInd, fVal)}
        }.toArray.groupBy(identity).mapValues(x=>cNum.fromInt(x.length)).toIterator, true).
      reduceByKey(cNum.plus).map{case ((label,featInd,featVal),freq) => (FHTuple2(label,featInd),(featVal,freq))}.
      cache()

    //here we add pointers to cached RDDs to be unpersisted later when not useful anymore
    rddToUnpersist += featWithinLabelDistribution

    //Sum frequncies of positive-values (any featVal>0) per for every pair (label, featInd)
    val featWithinLabelSubtotals =
      featWithinLabelDistribution.
        mapValues(_._2).
        reduceByKey(cNum.plus).
        partitionBy(defaultPartitioner)

    //Bulid cartesian product of labels and feature indices
    val labelsFeaturesCartesian =
      //computes all available feature indices in the dataset (unique)
      //those who have only zero-value across all labels are igonred
      dataset.
      mapPartitions(_.flatMap(_.features.indices).toSet.toIterator,false).   //compute distinct locally first, then flatten
      distinct(dataset.sparkContext.defaultParallelism).
      //build cartesian product with labels
      flatMap(featInd =>
      bcTotalLabelCounts.value.keys.map( label => (FHTuple2(label,featInd),null) )
    ).partitionBy(defaultPartitioner)

    //Compute zero-elements frequencies by subtracting positive-values frequencies from total counts (by label)
    val zeroElementsWithinLabelCounts =
      (labelsFeaturesCartesian leftOuterJoin featWithinLabelSubtotals).mapPartitions(_.map{
        case (FHTuple2(label,featInd),(_,optFreq)) =>
          (FHTuple2(label,featInd), cNum.minus(bcTotalLabelCounts.value.get(label).get,optFreq.getOrElse(cNum.zero)))
      },preservesPartitioning = true).
        collect{
          case (FHTuple2(label,featInd),diff) if cNum.gt(diff,cNum.zero) => (FHTuple2(label,featInd),(fNum.zero,diff))
          case (FHTuple2(label,featInd),diff) if cNum.lt(diff,cNum.zero) => throw new IllegalStateException("negative difference!")
        }

    featWithinLabelDistribution union zeroElementsWithinLabelCounts map{
      case (FHTuple2(label,featInd),(featVal,freq)) => RHTuple2((label,featInd),(featVal,freq))
    }
  }

  /**
    * enumerate all possible splits of a specific feature w.r.t to given
    * histograms of the supervision classes for each of the feature's values
    * @param valueStats a map between each of the feature values and the histogram of the supervision classes for this value
    * @return a sequence of the format [(Feature_Values_Set1, Feature_Values_Set2), (Class_Histogram_In_Set1, Class_Histogram_In_Set2))]
    */
  private def permute(valueStats:Map[FV,SparseCategorialVector[CV]])(implicit num: Numeric[CV]) = {
    val elemSet = valueStats.keys.toSet

    val elemSetWithIdx = elemSet.zipWithIndex
    val n = elemSet.size
    val splitsDecimals = 1 until pow(2, n - 1)

    // compute histogram of supervision class counts for a set of feature values
    def histForFeatValuesSet(set: Set[FV]) = set.map(valueStats).reduce(_+_)

    splitsDecimals.map{ dec =>
      val (leftSet,rightSet) = elemSetWithIdx.partition{case (value,index) => (dec / pow(2,index)) % 2 == 1}
      val leftFeatureValues = leftSet.map(_._1)
      val rightFeatureValues = rightSet.map(_._1)
      ((leftFeatureValues, rightFeatureValues), (histForFeatValuesSet(leftFeatureValues), histForFeatValuesSet(rightFeatureValues)))
    }
  }
}

object SparseDecisionTree{
  def train[FV: Numeric, CV : TypeTag: ClassTag: Numeric](
                                                 data: RDD[SlimLabeledPoint[FV]],
                                                 stopingCrateria: StoppingCriterion,
                                                 impurity: ImpurityFunction[CV]) = {
    val labels = data.map(_.label).max() + 1
    new SparseDecisionTree[FV,CV](labels,stopingCrateria,impurity).run(data)
  }

  /** IO FUNCTIONS **/
  def save[FV](sc: SparkContext, tree: SDTRoot[FV], path: String) = sc.parallelize(Seq(tree),1).saveAsObjectFile(path)
  def load[FV](sc: SparkContext, path: String) = sc.objectFile[SDTRoot[FV]](path).first()

  //TODO: Implement pruning w.r.t a validation set
  def prune[FV](sc: SparkContext, tree: SDTRoot[FV], validation: RDD[SlimLabeledPoint[FV]]) = ???
//  implicit def UnwrappedArray[T:ClassTag](warr: scala.collection.mutable.WrappedArray[T]): Array[T] = warr.toArray

}


