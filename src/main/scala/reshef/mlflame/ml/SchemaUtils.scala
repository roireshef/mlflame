package reshef.mlflame.ml

import org.apache.spark.sql.types.{StructField, DataType, StructType, NumericType}

/**
 * Created by roir on 03/08/2016.
 */
object SchemaUtils {
  def checkColumnType(schema: StructType, colName: String, dataType: DataType, parent:String) = {
    require(schema(colName).dataType.equals(dataType),
      s"checkColumnType failed in $parent: $colName (${schema(colName).dataType}) is not $dataType")
  }
  def checkNumericType(schema: StructType, colName: String, parent: String) = {
    require(schema(colName).dataType.isInstanceOf[NumericType],
      s"checkColumnType failed in $parent: $colName (${schema(colName).dataType}) is not numeric")
  }

  /**
   * Copied from org.apache.spark.ml.util
   */
  def appendColumn(
                    schema: StructType,
                    colName: String,
                    dataType: DataType): StructType = {
    if (colName.isEmpty) return schema
    val fieldNames = schema.fieldNames
    require(!fieldNames.contains(colName), s"Column $colName already exists.")
    val outputFields = schema.fields :+ StructField(colName, dataType, nullable = false)
    StructType(outputFields)
  }

  def dropColumn(
                    schema: StructType,
                    colName: String): StructType = {
    StructType(schema.filterNot(_.name == colName))
  }
}
