package reshef.mlflame.dtree.base

import reshef.mlflame.dtree.dto.SlimSparseVector

/**
 * A base class for Sparse Decision Tree nodes
 */
abstract class SDTNode[FV](
                            val stats: NodeStats,
                            val lChild: Option[SDTNode[FV]],
                            val rChild: Option[SDTNode[FV]]) extends Serializable{
  /**
    * predicts a class and its confidence level (probability) using the histogram of
    * the training examples in the assigned leaf
    * @param fVector  vector of features to be used for prediction
    * @return         tuple (class, confidence) of the predicted class
    */
  protected[dtree]
  def predict(fVector: SlimSparseVector[FV]): (Int, Double)

  //todo: fill return (understand why the option here)
  /**
    * recursively builds a set of the subtree's nodes after mapping them to a desired format
    * @param transform  a transformation to apply on nodes
    * @tparam U         the desired output type for nodes
    * @return
    */
  protected[dtree]
  def map[U](transform: SDTNode[FV] => Option[U]): Set[Option[U]] = {
    Set(transform(this)) ++
      (if (lChild.isDefined) lChild.get.map(transform) else Nil) ++
      (if (lChild.isDefined) rChild.get.map(transform) else Nil)
  }
}