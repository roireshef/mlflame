package reshef.mlflame.utils

import reflect.runtime.universe._
import reflect.ClassTag

/**
 * Created by roir on 03/08/2016.
 */
object Reflection {
  def typeToClassTag[T: TypeTag]: ClassTag[T] = {
    ClassTag[T]( typeTag[T].mirror.runtimeClass( typeTag[T].tpe ) )
  }

  def castLongTo[@specialized(Int, Long) V](num: Long)(implicit ct: ClassTag[V]): V = ct.runtimeClass match {
    case x if x == classOf[Int] => num.toInt.asInstanceOf[V]
    case x if x == classOf[Long] => num.asInstanceOf[V]
  }

  def fromDouble[@specialized(Int, Long) V](d:Double)(implicit ev: ClassTag[V]):V = ev match{
    case x:ClassTag[Int] => d.toInt.asInstanceOf[V]
    case x:ClassTag[Long] => d.toLong.asInstanceOf[V]
  }
}
