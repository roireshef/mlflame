package reshef.mlflame.dtree.dto

/**
  * A base trait for a feature vector
  */
trait SlimSparseVector[V] /*extends Ordering[SlimSparseVector[V]]*/{
  val size: Int
  val indices: Array[Int]
  val values: Array[V]

  def +[V2](that: SlimSparseVector[V2]): SlimSparseVector[V]
  def apply(index: Int): Option[V]
  def zipped: Stream[(Int, V)]
}

trait SortedByIndex{
  val indices: Array[Int]
  assertSortedIndices()

  /** assert that the indices are sorted in increasing order **/
  protected def assertSortedIndices() =
  1 until indices.length map(i=>indices(i)>indices(i-1)) forall (_==true) match {
    case false =>
      throw new Exception(this.getClass.getSimpleName + " should be initialized with an increasing indices array. " +
        s"Instead, got ${indices.mkString("[",", ","]")}")
    case _ =>
  }
}
