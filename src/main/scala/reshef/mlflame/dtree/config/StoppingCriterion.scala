package reshef.mlflame.dtree.config

import reshef.mlflame.dtree.impl.SplitStats

/**
 * Configuration holder class for Decision Tree's stopping criterion
 */
case class StoppingCriterion (
                               minBucket: Option[Int] = None,
                               minGain: Option[Double] = None,
                               maxDepth: Option[Int] = None){
  /**
    * Checks whether a splitting node meets the stopping criterion limits
    * @param splitStats Splitting node statistics
    * @tparam FV        Type of features
    * @tparam CV        Type of supervision class
    * @return           True if split meets the stopping criterion limits, False otherwise
    */
  def isSplitValid[FV,CV](splitStats: SplitStats[FV,CV])(implicit num: Numeric[CV]) = {
    Seq(
      minBucket match{
        case Some(sizeTh) =>
          num.toInt(splitStats.leftSubsetHistogram.values.sum) >= sizeTh &&
          num.toInt(splitStats.rightSubsetHistogram.values.sum) >= sizeTh
        case None => true
      },
      minGain match{
        case Some(gainTh) =>
          splitStats.gain >= gainTh
        case None => true
      },
      maxDepth match{
        case Some(depthTh) =>
          splitStats.depth < depthTh
        case None => true
      }
    ).reduce(_ && _)
  }
}
