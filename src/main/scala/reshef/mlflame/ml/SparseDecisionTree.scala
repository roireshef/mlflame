package reshef.mlflame.ml

import reshef.mlflame.utils.Implicits._
import reshef.mlflame.utils.Vectors
import reshef.mlflame.dtree.config.StoppingCriterion
import reshef.mlflame.dtree.dto.SlimLabeledPoint
import reshef.mlflame.dtree.impl.{SDTRoot, SplitStats}
import reshef.mlflame.dtree.{SparseDecisionTree => SDT, base, impl}
import reshef.mlflame.ml.SparseDecisionTree.FeatureType._
import reshef.mlflame.ml.SparseDecisionTree.{CountType, ImpurityFunction}
import org.apache.spark.ml.param._
import org.apache.spark.ml.util.Identifiable
import org.apache.spark.ml.{Estimator, Model}
import org.apache.spark.mllib.linalg.{Vector, VectorUDT}
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema
import org.apache.spark.sql.functions.{col, udf}
import org.apache.spark.sql.types.{DoubleType, IntegerType, StructType}
import org.apache.spark.sql.{DataFrame, Row}

import scala.reflect.ClassTag

/**
 * Created by roir on 03/08/2016.
 */
class SparseDecisionTree[FV:ClassTag:Numeric](
    featureVectorType: DataType with DataObjectType[FV],
    override val uid: String)
  extends Estimator[SparseDecisionTreeModel[FV]] with SparseDecisionTreeBase{

  def this(featureVectorType: DataType with DataObjectType[FV]) = this(featureVectorType, Identifiable.randomUID("SparseDecisionTree"))

  /** @group setParam */
  def setFeaturesCol(value: String): this.type = set(featuresCol, value)

  /** @group setParam */
  def setClassCol(value: String): this.type = set(classCol, value)

  /** @group setParam */
  def setNumClasses(value: Int): this.type = set(numClasses, value)

  /** @group setParam */
  def setStoppingCriterion(value: StoppingCriterion): this.type = set(stoppingCriterion, value)

  /** @group setParam */
  def setCountType(value: CountType.Value): this.type = set(countType, value)

  /** @group setParam */
  def setPredictionCol(value: String): this.type = set(optPredictionCol, Some(value))

  /** @group setParam */
  def setConfidenceCol(value: String): this.type = set(optConfidenceCol, Some(value))

  /** @group setParam */
  def setExtractionCol(value: String): this.type = set(optExtractionCol, Some(value))

  /** @group setParam */
  def setImpurityFunction(value: ImpurityFunction.Value): this.type = set(impurityFunction, value)

  override def fit(dataset: DataFrame): SparseDecisionTreeModel[FV] = {
    transformSchema(dataset.schema, logging = true)

    (featureVectorType, $(countType)) match {
      case (_:IsBinary, CountType.Int) =>
        val tree = SDT.train(prepareBinaryData(dataset), $(stoppingCriterion),getImpurity[Int])
        copyValues(new SparseDecisionTreeModel(uid, tree).asInstanceOf[SparseDecisionTreeModel[FV]].setParent(this))
      case (_:IsBinary, CountType.Long) =>
        val tree = SDT.train(prepareBinaryData(dataset), $(stoppingCriterion),getImpurity[Long])
        copyValues(new SparseDecisionTreeModel(uid, tree).asInstanceOf[SparseDecisionTreeModel[FV]].setParent(this))
      case (_:IsCategorial, CountType.Int) =>
        val tree = SDT.train(prepareCategorialData[FV](dataset),$(stoppingCriterion),getImpurity[Int])
        copyValues(new SparseDecisionTreeModel(uid, tree).setParent(this))
      case (_:IsCategorial, CountType.Long) =>
        val tree = SDT.train(prepareCategorialData[FV](dataset),$(stoppingCriterion),getImpurity[Long])
        copyValues(new SparseDecisionTreeModel(uid, tree).setParent(this))
    }

  }

  override def transformSchema(schema: StructType): StructType = {
    validateSchema(schema)
    schema
  }

  override def copy(extra: ParamMap) = defaultCopy(extra)

  private def prepareBinaryData(dataset: DataFrame) = {
    dataset.select(col($(featuresCol)), col($(classCol)).cast(IntegerType)).
      rdd.mapPartitions(_.map{case Row(ft: Vector, cl: Int) => SlimLabeledPoint.ofBinaryFeatures(cl,ft)}, true)
  }

  private def prepareCategorialData[FV:ClassTag:Numeric](dataset: DataFrame) = {
    dataset.select(col($(featuresCol)), col($(classCol)).cast(IntegerType)).
      rdd.mapPartitions(_.map{case Row(ft: Vector, cl: Int) => SlimLabeledPoint.ofCategorialFeatures[FV](cl,ft)}, true)
  }

  private def getImpurity[CV:Numeric]: base.Impurity.ImpurityFunction[CV] = {
    $(impurityFunction) match{
      case ImpurityFunction.Gini => impl.Impurity.Gini[CV]
      case ImpurityFunction.Entropy => impl.Impurity.Entropy[CV]($(numClasses))
    }
  }
}


class SparseDecisionTreeModel[FV:ClassTag:Numeric] private[ml](
    override val uid: String,
    final val tree: SDTRoot[FV])
  extends Model[SparseDecisionTreeModel[FV]] with SparseDecisionTreeBase {

  override def copy(extra: ParamMap): SparseDecisionTreeModel[FV] = {
    val copied = new SparseDecisionTreeModel(uid, tree)
    copyValues(copied, extra).setParent(parent)
  }

  private lazy val extractedIndices = tree.flatMap(_.stats match {
    case sp: SplitStats[_, _] => Some(sp.featureIndex)
    case _ => None
  }).toSeq

  def printTree = println(tree.toString())

  def getExtractedMapping = extractedIndices.zipWithIndex.map(_.swap).toMap.map(x=>x)

  override def transform(dataset: DataFrame): DataFrame = {
    validateField(dataset.schema, "featuresCol")
    transformSchema(dataset.schema, logging = true)

    val predict = udf{ vec:Vector =>
      tree.predict(SlimLabeledPoint.ofCategorialFeatures[FV](-1,vec).features)
    }
    val getPrediction = udf{ (row:GenericRowWithSchema) => row.getInt(0) }
    val getConfidence = udf{ (row:GenericRowWithSchema) => row.getDouble(1) }

    val extract = udf{ vec:Vector => Vectors.extract(vec,extractedIndices) }

    val afterPrediction = ($(optPredictionCol), $(optConfidenceCol)) match{
      case (None, None) => dataset
      case (Some(predictionCol), None) => dataset.withColumn(predictionCol, getPrediction(predict(col($(featuresCol)))))
      case (None, Some(confidenceCol)) => dataset.withColumn(confidenceCol, getConfidence(predict(col($(featuresCol)))))
      case (Some(predictionCol), Some(confidenceCol)) =>
        dataset.withColumn(s"result__$uid", predict(col($(featuresCol)))).
          withColumn(predictionCol, getPrediction(col(s"result__$uid"))).
          withColumn(confidenceCol, getConfidence(col(s"result__$uid"))).
          drop(s"result__$uid")
    }
    if ($(optExtractionCol).isDefined)
      afterPrediction.withColumn($(optExtractionCol).get, extract(col($(featuresCol))))
    else
      afterPrediction
  }

  override def transformSchema(schema: StructType): StructType = {
    schema.
      conditionOn($(optPredictionCol))(SchemaUtils.appendColumn(_,_,IntegerType), identity).
      conditionOn($(optConfidenceCol))(SchemaUtils.appendColumn(_,_,DoubleType), identity).
      conditionOn($(optExtractionCol))(SchemaUtils.appendColumn(_,_,new VectorUDT), identity)
  }
}

protected trait SparseDecisionTreeBase
  extends Params with HasParamValidation with HasFeaturesCol with HasClassCol
  with OptPredictionCol with OptConfidenceCol with OptExtractionCol{

  final val numClasses = new IntParam(
    this, "numClasses", "number of target classes (class values must be on the range [0, numClasses-1])")

  /** @group getParam */
  def getNumClasses: Int = $(numClasses)

  final val stoppingCriterion = new Param[StoppingCriterion](
    this, "stoppingCriterion", "stopping criterion for decision tree training")

  /** @group getParam */
  def getStoppingCriterion: StoppingCriterion = $(stoppingCriterion)

  setDefault(stoppingCriterion, StoppingCriterion())

  final val countType = new Param[CountType.Value](
    this, "countType", "whether to generate counts (during training) of type int (default) or long (for extremely big data sets)")

  /** @group getParam */
  def getCountType: CountType.Value = $(countType)

  setDefault(countType, CountType.Int)

  final val impurityFunction = new Param[ImpurityFunction.Value](
    this, "impurityFunction", "impurity function for computing information gain on splitting nodes")

  /** @group getParam */
  def getImpurityFunction: ImpurityFunction.Value = $(impurityFunction)

  setDefault(impurityFunction, ImpurityFunction.Gini)
}

object SparseDecisionTree{
  object FeatureType{
    sealed trait DataType
    sealed trait IsBinary extends DataType
    sealed trait IsCategorial extends DataType

    sealed trait DataObjectType[+T]

    final case object BinaryInt extends DataObjectType[Int] with IsBinary
    final case object CategorialInt extends DataObjectType[Int] with IsCategorial
    final case object CategorialLong extends DataObjectType[Long] with IsCategorial
  }

  object CountType extends Enumeration{
    val Int, Long  = Value
  }

  object ImpurityFunction extends Enumeration{
    val Entropy, Gini  = Value
  }
}
