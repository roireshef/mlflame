package reshef.mlflame.dtree

import reshef.mlflame.utils.SparkTest
import reshef.mlflame.dtree.config.StoppingCriterion
import reshef.mlflame.dtree.dto.{SlimLabeledPoint, SparseBinaryVector, SparseCategorialVector}
import reshef.mlflame.dtree.impl.{Impurity, SplitStats}
import org.apache.spark.Logging

/**
 * Created by roir on 09/06/2016.
 */
class TestDTree extends SparkTest(6,"TestDTree") with Logging{
  "CategorialDTree" should "split well (depth = 1)" in {
    val dataset = sc.parallelize(
      List(
        new SlimLabeledPoint(0,SparseCategorialVector(4,Array(0,  2,3), Array(1L,1L,1L))),
        new SlimLabeledPoint(0,SparseCategorialVector(4,Array(  1,2,3), Array(1L,1L,1L))),
        new SlimLabeledPoint(0,SparseCategorialVector(4,Array(  1,2,3), Array(1L,1L,1L))),
        new SlimLabeledPoint(0,SparseCategorialVector(4,Array(0,1,2,3), Array(1L,1L,1L,1L))),
        new SlimLabeledPoint(0,SparseCategorialVector(4,Array(0,1,2,3), Array(1L,1L,1L,1L))),
        new SlimLabeledPoint(1,SparseCategorialVector(4,Array(0,  2),   Array(1L,1L))),
        new SlimLabeledPoint(1,SparseCategorialVector(4,Array(0,1,2),   Array(1L,1L,1L))),
        new SlimLabeledPoint(1,SparseCategorialVector(4,Array(0,1,2),   Array(1L,1L,1L))),
        new SlimLabeledPoint(1,SparseCategorialVector(4,Array(0,1,2),   Array(1L,1L,1L))),
        new SlimLabeledPoint(1,SparseCategorialVector(4,Array(  1,2,3), Array(1L,1L,1L)))
      )
    )

    val entropy = Impurity.Entropy[Long](2)

    val tree = SparseDecisionTree.train(dataset,StoppingCriterion(None,None,Some(1)), entropy)

    info(tree.toString)

    assert(tree.stats.isInstanceOf[SplitStats[Long,Long]])
    assert(tree.stats.asInstanceOf[SplitStats[Long,Long]].featureIndex == 3)
    assert(tree.stats.asInstanceOf[SplitStats[Long,Long]].gain ==
      1 - (0.4 * entropy(Array(4L)) + 0.6 * entropy(Array(1L,5L))))
  }

  it should "split well (depth = 2)" in {

    val dataset = sc.parallelize(
      List.fill(10000)(
        List(
          new SlimLabeledPoint(0,SparseCategorialVector(4,Array(0,  2,3), Array(1L,1L,1L))),
          new SlimLabeledPoint(0,SparseCategorialVector(4,Array(  1,2,3), Array(1L,1L,1L))),
          new SlimLabeledPoint(0,SparseCategorialVector(4,Array(  1,2,3), Array(1L,1L,1L))),
          new SlimLabeledPoint(0,SparseCategorialVector(4,Array(0,1,2,3), Array(1L,1L,1L,1L))),
          new SlimLabeledPoint(0,SparseCategorialVector(4,Array(0,1,2,3), Array(1L,1L,1L,1L))),
          new SlimLabeledPoint(1,SparseCategorialVector(4,Array(0,  2),   Array(1L,1L))),
          new SlimLabeledPoint(1,SparseCategorialVector(4,Array(0,1,2),   Array(1L,1L,1L))),
          new SlimLabeledPoint(1,SparseCategorialVector(4,Array(0,1,2),   Array(1L,1L,1L))),
          new SlimLabeledPoint(1,SparseCategorialVector(4,Array(0,1,2),   Array(1L,1L,1L))),
          new SlimLabeledPoint(1,SparseCategorialVector(4,Array(  1,2,3), Array(1L,1L,1L)))
        )
      ).flatten
    )

    val tree = SparseDecisionTree.train(dataset,StoppingCriterion(None,Some(0.1),Some(2)), Impurity.Gini[Long])

    info(tree.toString)

    assert(tree.stats.isInstanceOf[SplitStats[Long,Long]])
    assert(tree.stats.asInstanceOf[SplitStats[Long,Long]].featureIndex == 3)
  }

  "BinaryDTree" should "split well (depth = 2)" in {

    val dataset = sc.parallelize(
      List.fill(10000)(
        List(
          new SlimLabeledPoint(0,SparseBinaryVector(4,Array(0,  2,3))),
          new SlimLabeledPoint(0,SparseBinaryVector(4,Array(  1,2,3))),
          new SlimLabeledPoint(0,SparseBinaryVector(4,Array(  1,2,3))),
          new SlimLabeledPoint(0,SparseBinaryVector(4,Array(0,1,2,3))),
          new SlimLabeledPoint(0,SparseBinaryVector(4,Array(0,1,2,3))),
          new SlimLabeledPoint(1,SparseBinaryVector(4,Array(0,  2))),
          new SlimLabeledPoint(1,SparseBinaryVector(4,Array(0,1,2))),
          new SlimLabeledPoint(1,SparseBinaryVector(4,Array(0,1,2))),
          new SlimLabeledPoint(1,SparseBinaryVector(4,Array(0,1,2))),
          new SlimLabeledPoint(1,SparseBinaryVector(4,Array(  1,2,3)))
        )
      ).flatten
    )

    val tree = SparseDecisionTree.train(dataset,StoppingCriterion(None,Some(0.1),Some(2)),Impurity.Gini[Long])

    info(tree.toString)

    assert(tree.stats.isInstanceOf[SplitStats[Long,Long]])
    assert(tree.stats.asInstanceOf[SplitStats[Long,Long]].featureIndex == 3)
  }


  "CategorialDTree" should "split first dimension well on perfect (multivalued) split" in {

    val dataset = sc.parallelize(
      List(
        new SlimLabeledPoint(1,SparseCategorialVector(4,Array(0,  2,3), Array(1L,   1L,1L))),
        new SlimLabeledPoint(1,SparseCategorialVector(4,Array(  1,2,3), Array(   1L,1L,1L))),
        new SlimLabeledPoint(1,SparseCategorialVector(4,Array(  1,2,3), Array(   1L,1L,1L))),
        new SlimLabeledPoint(1,SparseCategorialVector(4,Array(0,1,2,3), Array(1L,1L,1L,2L))),
        new SlimLabeledPoint(1,SparseCategorialVector(4,Array(0,1,2,3), Array(1L,1L,1L,2L))),
        new SlimLabeledPoint(2,SparseCategorialVector(4,Array(0,  2),   Array(1L,   1L   ))),
        new SlimLabeledPoint(2,SparseCategorialVector(4,Array(0,1,2),   Array(1L,1L,1L   ))),
        new SlimLabeledPoint(2,SparseCategorialVector(4,Array(0,1,2),   Array(1L,1L,1L   ))),
        new SlimLabeledPoint(2,SparseCategorialVector(4,Array(0,1,2),   Array(1L,1L,1L   ))),
        new SlimLabeledPoint(2,SparseCategorialVector(4,Array(  1,2,3), Array(   1L,1L,3L)))
      )
    )

    val entropy = Impurity.Entropy[Long](2)

    val tree = SparseDecisionTree.train(dataset,StoppingCriterion(None,None,Some(1)),entropy)
    assert(tree.stats.isInstanceOf[SplitStats[Long,Long]])
    assert(tree.stats.asInstanceOf[SplitStats[Long,Long]].featureIndex == 3)
    assert(tree.stats.asInstanceOf[SplitStats[Long,Long]].leftSubsetValues.sum ==
      tree.stats.asInstanceOf[SplitStats[Long,Long]].rightSubsetValues.sum)
  }

  it should "Build tree with Int types" in {
    val dataset = sc.parallelize(
      List(
        new SlimLabeledPoint(1,SparseCategorialVector(4,Array(0,  2,3), Array(1,   1,1))),
        new SlimLabeledPoint(1,SparseCategorialVector(4,Array(  1,2,3), Array(   1,1,1))),
        new SlimLabeledPoint(1,SparseCategorialVector(4,Array(  1,2,3), Array(   1,1,1))),
        new SlimLabeledPoint(1,SparseCategorialVector(4,Array(0,1,2,3), Array(1,1,1,2))),
        new SlimLabeledPoint(1,SparseCategorialVector(4,Array(0,1,2,3), Array(1,1,1,2))),
        new SlimLabeledPoint(2,SparseCategorialVector(4,Array(0,  2),   Array(1,   1   ))),
        new SlimLabeledPoint(2,SparseCategorialVector(4,Array(0,1,2),   Array(1,1,1   ))),
        new SlimLabeledPoint(2,SparseCategorialVector(4,Array(0,1,2),   Array(1,1,1   ))),
        new SlimLabeledPoint(2,SparseCategorialVector(4,Array(0,1,2),   Array(1,1,1   ))),
        new SlimLabeledPoint(2,SparseCategorialVector(4,Array(  1,2,3), Array(   1,1,3)))
      )
    )

    val entropy = Impurity.Entropy[Int](2)

    val tree = SparseDecisionTree.train(dataset,StoppingCriterion(None,None,Some(1)),entropy)
    assert(tree.stats.isInstanceOf[SplitStats[Int,Int]])
    assert(tree.stats.asInstanceOf[SplitStats[Int,Int]].featureIndex == 3)
    assert(tree.stats.asInstanceOf[SplitStats[Int,Int]].leftSubsetValues.sum ==
      tree.stats.asInstanceOf[SplitStats[Int,Int]].rightSubsetValues.sum)
  }

  it should "train and predict successfuly" in {
    val dataset = sc.parallelize(
      List(
        new SlimLabeledPoint(1,SparseCategorialVector(4,Array(0,  2,3), Array(1,   1,1))),
        new SlimLabeledPoint(1,SparseCategorialVector(4,Array(  1,2,3), Array(   1,1,1))),
        new SlimLabeledPoint(1,SparseCategorialVector(4,Array(  1,2,3), Array(   1,1,1))),
        new SlimLabeledPoint(1,SparseCategorialVector(4,Array(0,1,2,3), Array(1,1,1,2))),
        new SlimLabeledPoint(1,SparseCategorialVector(4,Array(0,1,2,3), Array(1,1,1,2))),
        new SlimLabeledPoint(2,SparseCategorialVector(4,Array(0,  2),   Array(1,   1   ))),
        new SlimLabeledPoint(2,SparseCategorialVector(4,Array(0,1,2),   Array(1,1,1   ))),
        new SlimLabeledPoint(2,SparseCategorialVector(4,Array(0,1,2),   Array(1,1,1   ))),
        new SlimLabeledPoint(2,SparseCategorialVector(4,Array(0,1,2),   Array(1,1,1   ))),
        new SlimLabeledPoint(2,SparseCategorialVector(4,Array(  1,2,3), Array(   1,1,3)))
      )
    )

    val entropy = Impurity.Entropy[Int](2)

    val tree = SparseDecisionTree.train(dataset,StoppingCriterion(None,None,Some(1)),entropy)
    assert(tree.predict(SparseCategorialVector(4,Array(0,1,2),Array(1,1,1)))._1 == 2)
    assert(tree.predict(SparseCategorialVector(4,Array(3), Array(1)))._1 == 1)
  }
}


