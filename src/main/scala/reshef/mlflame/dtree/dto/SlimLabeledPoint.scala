package reshef.mlflame.dtree.dto

import org.apache.spark.mllib.linalg.Vector

import scala.reflect.ClassTag


/**
  * Placeholder for an observation in a dataset
  * @param label    the supervision class of the observation
  * @param features feature-vector of the observation
  * @tparam FV      type of features
  */
class SlimLabeledPoint[@specialized(Int, Long) FV] (
    val label: Int,
    val features: SlimSparseVector[FV])
  extends RHTuple2[Int, SlimSparseVector[FV]](label, features) with Serializable

/** Additional constructors for SlimLabeledPoint **/
object SlimLabeledPoint{
  def ofBinaryFeatures(label:Int, vector: Vector) =
    new SlimLabeledPoint[Int](label,SparseBinaryVector.fromVector(vector))
  def ofCategorialFeatures[TV:ClassTag:Numeric](label:Int, vector: Vector) = {
    new SlimLabeledPoint[TV](label,SparseCategorialVector.fromVector[TV](vector))
  }
}