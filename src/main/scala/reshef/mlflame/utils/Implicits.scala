package reshef.mlflame.utils

object Implicits {
  implicit class ConditionOnOption[O](obj: O){
    /** Syntactic sugar for apply either one of two operations on an object,
      * conditioned on another Option object **/
    def conditionOn[V,U](opt: Option[V])(ifDefined: (O,V)=>U, ifEmpty: (O)=>U) = {
      opt match{
        case Some(x) => ifDefined(obj,x)
        case None => ifEmpty(obj)
      }
    }
  }
}
