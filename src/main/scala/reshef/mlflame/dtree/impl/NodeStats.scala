package reshef.mlflame.dtree.impl

import reshef.mlflame.dtree.base.NodeStats
import reshef.mlflame.dtree.dto.SparseCategorialVector

/**
  * Placeholder for SDT splitting-node statistics
  * @param featureIndex           index of the feature being split
  * @param leftSubsetValues       left subset values of the split-feature
  * @param rightSubsetValues      right subset values of the split-feature
  * @param depth                  depth of the split in the tree
  * @param impurity               impurity value before the split
  * @param gain                   information gain of the split
  * @param leftSubsetHistogram    histogram of the classes in the left subset
  * @param rightSubsetHistogram   histogram of the classes in the right subset
  * @tparam FV                    type of features
  * @tparam CV                    type of supervision class
  */
case class SplitStats[FV:Numeric,CV:Numeric] (
                                               featureIndex: Int,
                                               leftSubsetValues: Set[FV],
                                               rightSubsetValues: Set[FV],
                                               depth: Int,
                                               impurity: Double,
                                               gain: Double,
                                               leftSubsetHistogram: SparseCategorialVector[CV],
                                               rightSubsetHistogram: SparseCategorialVector[CV]
                        ) extends NodeStats{
  protected [dtree] lazy val probability = (leftSubsetHistogram + rightSubsetHistogram).toProb.toDense.toArray
  override def hashCode = featureIndex
  override def toString() =
    s"Depth: $depth, Feature: $featureIndex [$leftSubsetValues, $rightSubsetValues], Impurity: $impurity, " +
    s"Gain: $gain, Counts: [${leftSubsetHistogram.values.sum}, ${rightSubsetHistogram.values.sum}]"
}

/**
  * Placeholder for SDT leaf statistics
  * @param depth    depth of the node
  * @param hist
  * @param impurity
  * @tparam V
  */
case class LeafStats[V](depth: Int, hist: SparseCategorialVector[V], impurity: Double) extends NodeStats{
  protected [dtree] lazy val probability = hist.toProb.toDense.toArray
  override def toString() = s"Depth: $depth, Impurity: $impurity, " +
    (hist.indices zip hist.values).sortBy(_._1).map(x=>s"${x._1}: ${x._2}").mkString("[",",","]")
}
