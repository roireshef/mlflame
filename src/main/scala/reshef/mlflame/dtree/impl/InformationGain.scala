package reshef.mlflame.dtree.impl

import reshef.mlflame.dtree.base.Impurity._

/**
 * An object that utilizes a computation of information-gain for a given impurity function
 */
private[dtree]
object InformationGain {
  /**
    * computes the <a href="https://en.wikipedia.org/wiki/Information_gain_in_decision_trees">information-gain</a>
    * gained from splitting a set into two subsets
    * @param impurity         An impurity function. either {@link reshef.matflame.dtree.impl.Impurity#Entropy} or
    *                         {@link reshef.matflame.dtree.impl.Impurity#Gini}
    * @param setHistogram     original set histogram
    * @param subset1Histogram 1st subset histogram
    * @param subset2Histogram 2nd subset histogram
    * @tparam V               type of supervision class
    * @return                 the information gain of the split (scalar)
    */
  def compute[V](impurity:ImpurityFunction[V])(setHistogram: Array[V], subset1Histogram: Array[V], subset2Histogram: Array[V])(implicit num: Numeric[V]) =
    impurity(setHistogram) - computeDelta(impurity)(subset1Histogram, subset2Histogram)

  /** computes the 2nd term in <a href="https://en.wikipedia.org/wiki/Information_gain_in_decision_trees">information-gain</a>
    * formula (sum of relative impurities of the subsets) */
  def computeDelta[V](impurity:ImpurityFunction[V])(subset1Histogram: Array[V], subset2Histogram: Array[V])(implicit num: Numeric[V]) = {
    val subsetCount1 = subset1Histogram.sum
    val subsetCount2 = subset2Histogram.sum
    lazy val setTotal = num.toDouble(num.plus(subsetCount1, subsetCount2))

    lazy val impurity1 = impurity(subset1Histogram)
    lazy val impurity2 = impurity(subset2Histogram)

    def lazyRelativeImpurity(count: V, total: Double, impurity: Double) =
      num.toDouble(count) / total * impurity

    (subsetCount1, subsetCount2) match {
      case (cnt1, cnt2) if num.equiv(cnt1, num.zero) => impurity2
      case (cnt1, cnt2) if num.equiv(cnt2, num.zero) => impurity1
      case (cnt1, cnt2) =>
        lazyRelativeImpurity(subsetCount1, setTotal, impurity1) +
        lazyRelativeImpurity(subsetCount2, setTotal, impurity2)
    }
  }
}

