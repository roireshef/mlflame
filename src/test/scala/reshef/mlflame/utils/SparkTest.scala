package reshef.mlflame.utils

import org.apache.spark.sql.SQLContext
import org.apache.spark.{SparkConf, SparkContext}
import org.scalatest.{BeforeAndAfter, FlatSpec}

/**
 * Created by roir on 03/02/2016.
 */
abstract class SparkTest(cores: Int = 6, name: String = "example-spark") extends FlatSpec with BeforeAndAfter{
  private val master = s"local[$cores]"
  private val appName = name

  protected var sc: SparkContext = _
  protected var sql: SQLContext = _

  before {
    val conf = new SparkConf()
      .setMaster(master)
      .setAppName(appName)

    sc = SparkContext.getOrCreate(conf)
    sql = SQLContext.getOrCreate(sc)
  }

  after {
    /*if (sc != null) {
      sc.stop()
    }*/
  }
}
