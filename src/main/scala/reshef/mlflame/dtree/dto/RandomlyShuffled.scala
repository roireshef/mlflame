package reshef.mlflame.dtree.dto

/**
  * Overrides an object hash code with a random number generated at instantiation
  */
trait RandomlyShuffled{
  protected val _hashCode = (math.random * Int.MaxValue).toInt
  override def hashCode: Int = _hashCode
}
