package reshef.mlflame.dtree.base

/**
  * Base class for Node Statistics for Decision Tree models
  */
abstract class NodeStats{
  protected [dtree] val probability: Array[Double]
  val depth: Int
  val impurity: Double
}